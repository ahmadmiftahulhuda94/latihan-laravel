<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = new Cast;

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->save();
        return redirect()->route('admin.cast.index');
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]);
        return redirect()->route('admin.cast.index');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect()->route('admin.cast.index');
    }
}
