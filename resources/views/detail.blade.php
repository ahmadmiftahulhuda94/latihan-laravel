@extends('layout.catalog')

@section('page-content')
    <!-- details -->
    <section class="section details">
        <!-- details background -->
        <div class="details__bg" data-bg="{{ asset('frontend') }}/img/home/home__bg.jpg"></div>
        <!-- end details background -->

        <!-- details content -->
        <div class="container">
            <div class="row">
                <!-- title -->
                <div class="col-12">
                    <h1 class="details__title">I Dream in Another Language</h1>
                </div>
                <!-- end title -->

                <!-- content -->
                <div class="col-12">
                    <div class="card card--details">
                        <div class="row">
                            <!-- card cover -->
                            <div class="col-12 col-sm-4">
                                <div class="card__cover">
                                    <img src="{{ asset('frontend') }}/img/covers/cover.jpg" alt="">
                                </div>
                            </div>
                            <!-- end card cover -->

                            <!-- card content -->
                            <div class="col-12 col-sm-8">
                                <div class="card__content">

                                    <ul class="card__meta">
                                        <li><span>Genre:</span> <a href="#">Action</a>
                                            <a href="#">Triler</a>
                                        </li>
                                        <li><span>Release year:</span> 2017</li>
                                    </ul>

                                    <div class="card__description card__description--details">
                                        It is a long established fact that a reader will be distracted by the readable
                                        content of a page when looking at its layout. The point of using Lorem Ipsum is that
                                        it has a more-or-less normal distribution of letters, as opposed to using 'Content
                                        here, content here', making it look like readable English. Many desktop publishing
                                        packages and web page editors now use Lorem Ipsum as their default model text, and a
                                        search for 'lorem ipsum' will uncover many web sites still in their infancy.
                                    </div>
                                </div>
                            </div>
                            <!-- end card content -->
                        </div>
                    </div>
                </div>
                <!-- end content -->


            </div>
        </div>
        <!-- end details content -->
    </section>
    <!-- end details -->

    <!-- content -->
    <section class="content">
        <div class="content__head">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- content title -->
                        <h2 class="content__title">Discover</h2>
                        <!-- end content title -->

                        <!-- content tabs nav -->
                        <ul class="nav nav-tabs content__tabs" id="content__tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1"
                                    aria-selected="true">Comments</a>
                            </li>
                        </ul>
                        <!-- end content tabs nav -->

                        <!-- content mobile tabs nav -->
                        <div class="content__mobile-tabs" id="content__mobile-tabs">
                            <div class="content__mobile-tabs-btn dropdown-toggle" role="navigation" id="mobile-tabs"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <input type="button" value="Comments">
                                <span></span>
                            </div>

                            <div class="content__mobile-tabs-menu dropdown-menu" aria-labelledby="mobile-tabs">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item"><a class="nav-link active" id="1-tab" data-toggle="tab"
                                            href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true">Comments</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- end content mobile tabs nav -->
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 col-xl-8">
                    <!-- content tabs -->
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="1-tab">
                            <div class="row">
                                <!-- comments -->
                                <div class="col-12">
                                    <div class="comments">
                                        <ul class="comments__list">

                                            <li class="comments__item">
                                                <div class="comments__autor">
                                                    <img class="comments__avatar"
                                                        src="{{ asset('frontend') }}/img/user.png" alt="">
                                                    <span class="comments__name">John Doe</span>
                                                    <span class="comments__time">07.08.2018, 14:33</span>
                                                </div>
                                                <p class="comments__text">There are many variations of passages of Lorem
                                                    Ipsum available, but the majority have suffered alteration in some form,
                                                    by injected humour, or randomised words which don't look even slightly
                                                    believable. If you are going to use a passage of Lorem Ipsum, you need
                                                    to be sure there isn't anything embarrassing hidden in the middle of
                                                    text.</p>
                                            </li>
                                        </ul>

                                        <form action="#" class="form">
                                            <textarea id="text" name="text" class="form__textarea"
                                                placeholder="Add comment"></textarea>
                                            <button type="button" class="form__btn">Send</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- end comments -->
                            </div>
                        </div>
                    </div>
                    <!-- end content tabs -->
                </div>


            </div>
        </div>
    </section>
    <!-- end content -->

@endsection
