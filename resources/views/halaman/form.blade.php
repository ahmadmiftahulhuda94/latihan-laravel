@extends('layout.master')

@section('Judul')
Buat Account Baru!
@endsection

@section('content')
        <h1>Buat Account Baru!</h1>
        <h3>Sign up form</h3>
    <form action="/welcome" method="post">
        @csrf
        <p>First Name:</p>
        <input type="text">
        <p>Last Name:</p>
        <input type="text" name="nama"><br><br>
        <label>Alamat</label><br><br>
        <textarea name="address" cols="30" rows="10"></textarea><br><br>
        <p>Gender:</p>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br>
        <p>Nationallity:</p>
        <select>
            <option>Indonesia</option>
            <option>Malaysia</option>
            <option>Brunei</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br>
        <p>Bio:</p>
        <textarea cols="25" rows="7"></textarea><br><br>
        <input type="submit"value="kirim">
    </form>
@endsection