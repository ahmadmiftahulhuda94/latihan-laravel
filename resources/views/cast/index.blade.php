@extends('layout.master')

@section('judul')
    Halaman List Cast
@endsection

@section('content')
    <a href="{{ route('admin.cast.create') }}" class="btn btn-success mb-3">Tambah Data</a>
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=> $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>{{ $item->bio }}</td>
                    <td>
                        <form action="{{ route('admin.cast.delete', ['id' => $item->id]) }}" method="POST">
                            @method('delete')
                            @csrf
                            <a href="{{ route('admin.cast.show', ['id' => $item->id]) }}"
                                class="btn btn-info btn-sm">Detail</a>
                            <a href="{{ route('admin.cast.edit', ['id' => $item->id]) }}"
                                class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td>Data masih kosong</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
