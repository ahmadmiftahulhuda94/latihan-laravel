@extends('layout.catalog')

@section('page-content')
    <!-- home -->
    <section class="home">
        <!-- home bg -->
        <div class="owl-carousel home__bg">
            <div class="item home__cover" data-bg="{{ asset('frontend') }}/img/home/home__bg.jpg"></div>
            <div class="item home__cover" data-bg="{{ asset('frontend') }}/img/home/home__bg2.jpg"></div>
            <div class="item home__cover" data-bg="{{ asset('frontend') }}/img/home/home__bg3.jpg"></div>
            <div class="item home__cover" data-bg="{{ asset('frontend') }}/img/home/home__bg4.jpg"></div>
        </div>
        <!-- end home bg -->
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="home__title"><b>Baru</b> ditambahkan</h1>

                    <button class="home__nav home__nav--prev" type="button">
                        <i class="icon ion-ios-arrow-round-back"></i>
                    </button>
                    <button class="home__nav home__nav--next" type="button">
                        <i class="icon ion-ios-arrow-round-forward"></i>
                    </button>
                </div>

                <div class="col-12">
                    <div class="owl-carousel home__carousel">
                        <div class="item">
                            <!-- card -->
                            <div class="card card--big">
                                <div class="card__cover">
                                    <img src="{{ asset('frontend') }}/img/covers/cover.jpg" alt="">
                                    <a href="#" class="card__play">
                                        <i class="icon ion-ios-open"></i>
                                    </a>
                                </div>
                                <div class="card__content">
                                    <h3 class="card__title"><a href="#">I Dream in Another Language</a></h3>
                                    <span class="card__category">
                                        <a href="#">Action</a>
                                        <a href="#">Triler</a>
                                    </span>
                                    <span class="card__rate"><i class="icon ion-ios-star"></i>8.4</span>
                                </div>
                            </div>
                            <!-- end card -->
                        </div>

                        <div class="item">
                            <!-- card -->
                            <div class="card card--big">
                                <div class="card__cover">
                                    <img src="{{ asset('frontend') }}/img/covers/cover2.jpg" alt="">
                                    <a href="#" class="card__play">
                                        <i class="icon ion-ios-open"></i>
                                    </a>
                                </div>
                                <div class="card__content">
                                    <h3 class="card__title"><a href="#">Benched</a></h3>
                                    <span class="card__category">
                                        <a href="#">Comedy</a>
                                    </span>
                                    <span class="card__rate"><i class="icon ion-ios-star"></i>7.1</span>
                                </div>
                            </div>
                            <!-- end card -->
                        </div>

                        <div class="item">
                            <!-- card -->
                            <div class="card card--big">
                                <div class="card__cover">
                                    <img src="{{ asset('frontend') }}/img/covers/cover3.jpg" alt="">
                                    <a href="#" class="card__play">
                                        <i class="icon ion-ios-open"></i>
                                    </a>
                                </div>
                                <div class="card__content">
                                    <h3 class="card__title"><a href="#">Whitney</a></h3>
                                    <span class="card__category">
                                        <a href="#">Romance</a>
                                        <a href="#">Drama</a>
                                    </span>
                                    <span class="card__rate"><i class="icon ion-ios-star"></i>6.3</span>
                                </div>
                            </div>
                            <!-- end card -->
                        </div>

                        <div class="item">
                            <!-- card -->
                            <div class="card card--big">
                                <div class="card__cover">
                                    <img src="{{ asset('frontend') }}/img/covers/cover4.jpg" alt="">
                                    <a href="#" class="card__play">
                                        <i class="icon ion-ios-open"></i>
                                    </a>
                                </div>
                                <div class="card__content">
                                    <h3 class="card__title"><a href="#">Blindspotting</a></h3>
                                    <span class="card__category">
                                        <a href="#">Comedy</a>
                                        <a href="#">Drama</a>
                                    </span>
                                    <span class="card__rate"><i class="icon ion-ios-star"></i>7.9</span>
                                </div>
                            </div>
                            <!-- end card -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end home -->

    <!-- content -->
    <section class="content">
        <div class="content__head">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="content__title">Daftar Film</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <!-- content tabs -->
            <div class="row">

                <?php
                for ($i=0; $i <6; $i++) {                                     
                ?>
                <!-- card -->
                <div class="col-6 col-sm-4 col-lg-3 col-xl-2">
                    <div class="card">
                        <div class="card__cover">
                            <img src="{{ asset('frontend') }}/img/covers/cover.jpg" alt="">
                            <a href="{{ route('public.movie.detail', ['id' => 1]) }}" class="card__play">
                                <i class="icon ion-ios-open"></i>
                            </a>
                        </div>
                        <div class="card__content">
                            <h3 class="card__title"><a href="#">I Dream in Another Language</a></h3>
                            <span class="card__category">
                                <a href="#">Action</a>
                                <a href="#">Triler</a>
                            </span>
                            <span class="card__rate"><i class="icon ion-ios-star"></i>8.4</span>
                        </div>
                    </div>
                </div>
                <!-- end card -->
                <?php                                    
                }    
                ?>






            </div>

            <div class="row">
                <div class="col-12">
                    <ul class="paginator">
                        <li class="paginator__item paginator__item--prev">
                            <a href="#"><i class="icon ion-ios-arrow-back"></i></a>
                        </li>
                        <li class="paginator__item paginator__item--active"><a href="#">1</a></li>
                        <li class="paginator__item"><a href="#">2</a></li>
                        <li class="paginator__item"><a href="#">3</a></li>
                        <li class="paginator__item"><a href="#">4</a></li>
                        <li class="paginator__item paginator__item--next">
                            <a href="#"><i class="icon ion-ios-arrow-forward"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- end content -->

@endsection
