<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600%7CUbuntu:300,400,500,700" rel="stylesheet">

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/nouislider.min.css">
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/plyr.css">
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/photoswipe.css">
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/default-skin.css">
    <link rel="stylesheet" href="{{ asset('frontend') }}/css/main.css">

    <!-- Favicons -->
    <link rel="icon" type="image/png" href="{{ asset('frontend') }}/icon/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon" href="{{ asset('frontend') }}/icon/favicon-32x32.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('frontend') }}/icon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('frontend') }}/icon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('frontend') }}/icon/apple-touch-icon-144x144.png">

    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Dmitry Volkov">
    <title>REPFilm</title>

</head>

<body class="body">

    <!-- header -->
    <header class="header">
        <div class="header__wrap">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="header__content">
                            <!-- header logo -->
                            <a href="{{ route('public.homepage') }}" class="header__logo">
                                REPFilm
                            </a>
                            <!-- end header logo -->

                            <!-- header nav -->
                            <ul class="header__nav">

                                {{-- <li class="header__nav-item">
                                    <a href="pricing.html" class="header__nav-link">Home</a>
                                </li>

                                <li class="header__nav-item">
                                    <a href="pricing.html" class="header__nav-link">Daftar Film</a>
                                </li> --}}

                            </ul>
                            <!-- end header nav -->

                            <!-- header auth -->
                            <div class="header__auth">
                                <button class="header__search-btn" type="button">
                                    <i class="icon ion-ios-search"></i>
                                </button>
                                <a href="{{ route('login') }}" class="header__sign-in">
                                    <i class="icon ion-ios-log-in"></i>
                                    <span>Masuk</span>
                                </a>
                                <a href="{{ route('register') }}" class="header__sign-in">
                                    <i class="icon ion-ios-log-in"></i>
                                    <span>Daftar</span>
                                </a>
                            </div>
                            <!-- end header auth -->

                            <!-- header menu btn -->
                            <button class="header__btn" type="button">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                            <!-- end header menu btn -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- header search -->
        <form action="#" class="header__search">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="header__search-content">
                            <input type="text" placeholder="Search for a movie, TV Series that you are looking for">

                            <button type="button">search</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- end header search -->
    </header>
    <!-- end header -->

    @yield('page-content')

    <!-- footer -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <!-- footer copyright -->
                <div class="col-12">
                    <div class="footer__copyright">
                        <small><a target="_blank" href="https://www.templateshub.net">Templates Hub</a></small>

                        <ul>
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
                <!-- end footer copyright -->
            </div>
        </div>
    </footer>
    <!-- end footer -->

    <!-- JS -->
    <script src="{{ asset('frontend') }}/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('frontend') }}/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('frontend') }}/js/owl.carousel.min.js"></script>
    <script src="{{ asset('frontend') }}/js/jquery.mousewheel.min.js"></script>
    <script src="{{ asset('frontend') }}/js/jquery.mCustomScrollbar.min.js"></script>
    <script src="{{ asset('frontend') }}/js/wNumb.js"></script>
    <script src="{{ asset('frontend') }}/js/nouislider.min.js"></script>
    <script src="{{ asset('frontend') }}/js/plyr.min.js"></script>
    <script src="{{ asset('frontend') }}/js/jquery.morelines.min.js"></script>
    <script src="{{ asset('frontend') }}/js/photoswipe.min.js"></script>
    <script src="{{ asset('frontend') }}/js/photoswipe-ui-default.min.js"></script>
    <script src="{{ asset('frontend') }}/js/main.js"></script>
</body>

</html>
