@extends('layout.master')
@section('judul')
    Halaman Detail Genre {{ $genre->id }}
@endsection

@section('content')

    <h3>{{ $genre->nama }}</h3>

    <div class="d-grid gap-2 d-md-block">
        <a href="{{ route('admin.genre.index') }}" class="btn btn-dark">Kembali</a>
    </div>
@endsection
