@extends('layout.master')
@section('judul')
    Tambah Genre
@endsection

@section('content')
    <form action="{{ route('admin.genre.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama Genre</label>
            <input type="text" name="nama" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
