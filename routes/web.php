<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();
Route::group(['prefix' => 'admin'], function () {

    Route::group(['middleware' => 'auth'], function () {

        Route::get('/', 'DashboardController@index')->name('admin.dashboard');

        //CRUD Cast
        Route::group(['prefix' => 'cast'], function () {
            Route::get('/', 'CastController@index')->name('admin.cast.index');

            Route::get('/show/{id}', 'CastController@show')->name('admin.cast.show');
            Route::get('/create', 'CastController@create')->name('admin.cast.create');
            Route::post('/store', 'CastController@store')->name('admin.cast.store');

            Route::get('/edit/{id}', 'CastController@edit')->name('admin.cast.edit');
            Route::put('/update/{id}', 'CastController@update')->name('admin.cast.update');

            Route::delete('/delete/{id}', 'CastController@destroy')->name('admin.cast.delete');
        });

        //CRUD Genre
        Route::group(['prefix' => 'genre'], function () {
            Route::get('/', 'GenreController@index')->name('admin.genre.index');

            Route::get('/show/{id}', 'GenreController@show')->name('admin.genre.show');
            Route::get('/create', 'GenreController@create')->name('admin.genre.create');
            Route::post('/store', 'GenreController@store')->name('admin.genre.store');

            Route::get('/edit/{id}', 'GenreController@edit')->name('admin.genre.edit');
            Route::put('/update/{id}', 'GenreController@update')->name('admin.genre.update');

            Route::delete('/delete/{id}', 'GenreController@destroy')->name('admin.genre.delete');
        });

        //CRUD Peran
        Route::group(['prefix' => 'peran'], function () {
            Route::get('/', 'PeranController@index')->name('admin.peran.index');

            Route::get('/create', 'PeranController@create')->name('admin.peran.create');
            Route::post('/store', 'PeranController@store')->name('admin.peran.store');

            Route::get('/edit/{id}', 'PeranController@edit')->name('admin.peran.edit');
            Route::put('/update/{id}', 'PeranController@update')->name('admin.peran.update');

            Route::delete('/delete/{id}', 'PeranController@destroy')->name('admin.peran.delete');
        });
    });
});

Route::get('/', 'Catalog\HomepageController@index')->name('public.homepage');
Route::get('/movie/{id}', 'Catalog\HomepageController@show')->name('public.movie.detail');
